import kivy
from kivy.app import App
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.graphics import Color, Ellipse, Rectangle
from kivy.uix.togglebutton import ToggleButton
from kivy.clock import Clock
from kivy.properties import ListProperty
from player import Player
from logic import Game

kivy.require('1.10.1')


class MyApp(App):

    def build(self):
        return UIManager()


class UIManager(BoxLayout):

    def __init__(self, **kwargs):
        super(UIManager, self).__init__(**kwargs)
        self.reset()

    def reset(self):
        self.clear_widgets()
        self.add_widget(GameSetupUI(self))

    def start_game(self, gameui):
        self.clear_widgets()
        self.add_widget(gameui)


class BoardUI(BoxLayout):

    def __init__(self, game, manager, **kwargs):
        super(BoardUI, self).__init__(**kwargs)

        self.orientation = 'vertical'
        self.board = GridLayout(
            rows=game.size, cols=game.size, size_hint=(1, 0.9))
        self.controls = ControlsUI(manager, game)
        self.add_widget(self.controls)
        self.add_widget(self.board)
        self.game = game
        self.locked = False

        for r in range(game.size):
            for c in range(game.size):
                tile = Button()
                tile.bind(on_press=lambda a=0, x=r, y=c: self._click(x, y))
                self.board.add_widget(tile)

    def _click(self, i, j):
        # print("Clicked on {}".format((i, j)))
        if not self.locked:
            self.game.click(i, j)

    def schedule(self, function, delay):
        Clock.schedule_once(function, delay)

    def paint(self):

        (x,y) = self.controls.player_label.pos
        with self.controls.player_label.canvas:
            Color(*self.game.players[self.game.active_player].rgb)
            Rectangle(pos=(x+5, y+5), size=(20,20))

        for r in range(self.board.rows):
            for c in range(self.board.cols):
                button = self.board.children[-1-r*self.board.cols-c]
                tile = self.game.tiles[r][c]
                (x, y) = button.pos
                (w, h) = button.size
                button.canvas.clear()
                with button.canvas:
                    Color(1, 1, 1)
                    Rectangle(pos=(x+2, y+2), size=(w-4, h-4))

                if tile.owner not in [None, 'luknja']:
                    we = 2*w//5
                    he = 2*h//5
                    xe = x+w//20
                    ye = y+h//20
                    with button.canvas:
                        Color(*self.game.players[tile.owner].rgb)
                        if tile.content > 0:
                            Ellipse(pos=(xe, ye+h//2), size=(we, he))
                        if tile.content > 1:
                            Ellipse(pos=(xe+w//2, ye+h//2), size=(we, he))
                        if tile.content > 2:
                            Ellipse(pos=(xe, ye), size=(we, he))
                        if tile.content > 3:
                            Ellipse(pos=(xe+w//2, ye), size=(we, he))

                elif tile.owner == 'luknja':
                    with button.canvas:
                        Color(0, 0, 0)
                        Ellipse(pos=(x+w//10, y+h//10), size=(4*w//5, 4*h//5))


class ControlsUI(BoxLayout):

    def __init__(self, manager, game, **kwargs):
        super(ControlsUI, self).__init__(**kwargs)

        self.orientation = 'horizontal'
        self.size_hint = (1, 0.1)

        self.player_label = Label(text='Active player:')
        self.add_widget(self.player_label)

        undo_button = Button(text='<')
        undo_button.bind(on_press=lambda x: game.load_state(game.saved_state))
        self.add_widget(undo_button)

        next_move_button = Button(text='>')
        next_move_button.bind(on_press=lambda x: game.play_turn())
        self.add_widget(next_move_button)

        simulation_button = Button(text='>>>')
        simulation_button.bind(on_press=lambda x: game.simulate())
        self.add_widget(simulation_button)

        new_game_button = Button(text='New game')
        new_game_button.bind(on_press=lambda x: manager.reset())
        self.add_widget(new_game_button)


class GameSetupUI(GridLayout):

    def __init__(self, manager, **kwargs):
        super(GameSetupUI, self).__init__(**kwargs)

        self.rows = 5
        self.columns = 4
        self.uimanager = manager

        self.players = ['disabled']*4
        self.colors = [(1, 0, 0), (0, 1, 0), (0, 0, 1), (1, 0.5, 0)]

        for i in range(4):
            label = Label()
            self.add_widget(label)
            with label.canvas:
                Color(*self.colors[i])
                Rectangle(pos=(2, 2), size=(40, 40))

            for val in ['disabled', 'computer', 'human']:
                btn = ToggleButton(text=val, group='player{}'.format(i))
                btn.bind(on_release=lambda btn, player=i:
                         self.set_player(player, btn.text))
                if val == 'disabled':
                    btn.state = 'down'
                self.add_widget(btn)

        btn = Button(text='Start game!')
        btn.bind(on_press=lambda x: self.start_game())
        self.add_widget(btn)

    def set_player(self, player, status):
        self.players[player] = status

    def start_game(self):
        players = []
        for i in range(4):
            if self.players[i] != 'disabled':
                players.append(
                    Player(len(players), self.colors[i], self.players[i])
                )
        game = Game(players)
        ui = BoardUI(game, self.uimanager)
        game.ui = ui
        self.uimanager.start_game(ui)
