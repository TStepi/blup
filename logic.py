class Tile():

    def __init__(self, orig=None):
        if orig:
            self.owner = orig.owner
            self.content = orig.content
        else:
            self.owner = None
            self.content = 0

    def __repr__(self):
        return "({},{})".format(self.owner, self.content)


class GameState():

    def __init__(self, tiles, live_players, active_player):
        self.tiles = [[Tile(tiles[i][j]) for j in range(len(tiles))]
                      for i in range(len(tiles))]
        self.live_players = [x for x in live_players]
        self.active_player = active_player


class Game():

    def __init__(self, players, size=7):
        self.size = 7
        self.tiles = [[Tile() for i in range(size)] for j in range(size)]
        self.tiles[size//2][size//2].owner = 'luknja'
        self.to_pop = set()
        self.ui = None
        self.simulation = False

        self.players = players
        self.num_players = len(players)
        self.live_players = [True]*self.num_players
        self.active_player = 0
        self.saved_state = self.current_state()

    def current_state(self):
        return GameState(self.tiles, self.live_players, self.active_player)

    def save_state(self):
        self.saved_state = self.current_state()

    def load_state(self, game_state):
        self.tiles = game_state.tiles
        self.live_players = game_state.live_players
        self.active_player = game_state.active_player
        if self.ui:
            self.ui.paint()

    def max_balls(self, r, c):
        if r in [0, self.size-1] and c in [0, self.size-1]:
            return 1
        elif r in [0, self.size-1] or c in [0, self.size-1]:
            return 2
        else:
            return 3

    def get_neighbours(self, r, c):
        lst = []
        if r > 0:
            lst.append((r-1, c))
        if r < (self.size-1):
            lst.append((r+1, c))
        if c > 0:
            lst.append((r, c-1))
        if c < (self.size-1):
            lst.append((r, c+1))
        return lst

    def check_players(self):
        self.live_players = [False]*4
        for r in range(self.size):
            for c in range(self.size):
                if self.tiles[r][c].owner not in ['luknja', None]:
                    self.live_players[self.tiles[r][c].owner] = True

        if self.live_players.count(True) < 2:
            self.game_over(self.active_player)

    def game_over(self, winner):
        self.simulation = False
        print("Player {} won the game!".format(winner))

    def simulate(self):
        self.simulation = not self.simulation
        if self.simulation:
            self.play_turn()

    def play_turn(self):
        p = self.players[self.active_player]
        if not p.is_human:
            self.click(*p.action_selector(self.current_state()))

    def finish_turn(self, popped=False):
        if popped:
            self.check_players()
        self.active_player = (self.active_player+1) % self.num_players
        while not self.live_players[self.active_player]:
            self.active_player = (self.active_player+1) % 4

        if self.simulation:
            if self.ui:
                self.ui.schedule(lambda x: self.play_turn(), 1.0)
            else:
                self.play_turn()

    def click(self, i, j):
        self.save_state()
        if self.tiles[i][j].owner in [None, self.active_player]:
            self.tiles[i][j].owner = self.active_player
            self.tiles[i][j].content += 1

            if self.ui:
                self.ui.paint()

            if self.tiles[i][j].content > self.max_balls(i, j):
                self.to_pop.add((i, j))
                self.pop()
            else:
                self.finish_turn()

    def pop(self):
        (r, c) = self.to_pop.pop()
        neighbours = self.get_neighbours(r, c)
        self.tiles[r][c].content -= len(neighbours)
        for (i, j) in neighbours:
            if self.tiles[i][j].owner != 'luknja':
                self.tiles[i][j].owner = self.tiles[r][c].owner
                self.tiles[i][j].content += 1
                if self.tiles[i][j].content > self.max_balls(i, j):
                    self.to_pop.add((i, j))

        if self.tiles[r][c].content == 0:
            self.tiles[r][c].owner = None

        if self.ui:
            self.ui.paint()

        if len(self.to_pop) > 0:
            if self.ui:
                self.ui.schedule(lambda x: self.pop(), 0.25)
            else:
                self.pop()
        else:
            self.finish_turn(popped=True)
