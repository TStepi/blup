import random


def random_action_selector(game_state):
    n = len(game_state.tiles) - 1
    while True:
        r = random.randint(0, n)
        c = random.randint(0, n)
        if game_state.tiles[r][c].owner in [game_state.active_player, None]:
            return (r, c)


class Player():

    def __init__(self, idx, rgb, brains):
        self.rgb = rgb
        self.idx = idx

        if brains == 'human':
            self.is_human = True
            self.action_selector = lambda x: None
        else:
            self.is_human = False
            self.action_selector = random_action_selector
